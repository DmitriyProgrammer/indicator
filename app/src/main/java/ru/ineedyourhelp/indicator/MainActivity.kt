package ru.ineedyourhelp.indicator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Math.abs

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myButt: Button = findViewById(R.id.buttonStart)

        myButt.setOnClickListener {
            var itrtr:Int = 100
            var tmp:Int = 0
            for(i in 1..100) {
                var rez = getIndi(itrtr)
                progressBar.setProgress(abs(rez))
            }
        }
    }

    fun getIndi(rnd:Int = 100): Int {
        var randomNumber = (0..10).random()
        var z:Int = 0
        var o:Int = 0
        for(i in 1..rnd){
            if( (0..1).random() == 1 ) {
                z++
            } else {
                o++
            }
        }
        return (z - o)
    }
}